const express = require('express');
const app=express();
const port=3033;
app.get('/',function(req,res){
    res.send('This is a sample for CICD Pipeline');
})
app.listen(port,function(){
    console.log('Application is running on port http://localhost:${port}');
})